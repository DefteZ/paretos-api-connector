# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed
- Fixed duplicated/missing items from get_evaluations and get_pareto_optimal_evaluations function in obtained result

## [1.1.0] - 2021-03-23
### Added
- Changelog (DEV-82)
- Data API for dashboard including a script to host the dashboard locally (DEV-55)

## [1.0.1] - 2021-03-16
### Fixed
- Due to problems with sqlalchemy update 1.3.x to 1.4.0 there was a error message all the time.

## [1.0.0] - 2021-03-11

[Unreleased]: https://gitlab.com/paretos-pubic/paretos-api-connector/-/compare?from=v1.1.0&to=master
[1.1.0]:https://gitlab.com/paretos-pubic/paretos-api-connector/-/compare?from=v1.0.1&to=1.1.0
[1.0.1]: https://gitlab.com/paretos-pubic/paretos-api-connector/-/compare?from=v1.0.0&to=1.0.1
[1.0.0]: https://gitlab.com/paretos-pubic/paretos-api-connector/-/tags/v1.0.0
