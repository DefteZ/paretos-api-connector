from typing import Union

from .maximum import Maximum
from .minimum import Minimum

Goal = Union[Minimum, Maximum]
