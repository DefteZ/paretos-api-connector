from paretos.exceptions import ParetosError


class ProjectAlreadyExists(ParetosError):
    pass
