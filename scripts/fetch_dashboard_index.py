import os
from pathlib import Path

import requests

DASHBOARD_VERSION = "1.0.2"
HOST = "https://cdn.paretos.io"


def get_dashboard_index():
    return f"{HOST}/dashboard/{DASHBOARD_VERSION}/index.html"


def get_app_public_index_path():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return Path(dir_path).parent / "paretos/app/templates/index.html"


r = requests.get(get_dashboard_index())

open(get_app_public_index_path(), "wb").write(r.content)
