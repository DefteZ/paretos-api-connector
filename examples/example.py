import configparser
import os
from datetime import datetime
from typing import Dict

from paretos import (
    Config,
    DesignParameter,
    EnvironmentInterface,
    KpiGoalMaximum,
    KpiGoalMinimum,
    KpiParameter,
    OptimizationProblem,
    Paretos,
    RunTerminator,
)

# THIS IS ONLY AN EXAMPLE HOW TO LOAD THE CONFIG PARAMETERS
# Feel free to use the way you are most familiar with!
init_config = configparser.ConfigParser()
config_file = os.path.realpath(os.path.join(os.getcwd(), "env.ini"))
init_config.read(config_file)
customer_token = init_config["EXAMPLE"]["CUSTOMER_TOKEN"].strip('"')

config = Config(
    customer_token=customer_token,
)

max_number_of_evaluations = 20

design_1 = DesignParameter(name="x", minimum=-5, maximum=5)
design_2 = DesignParameter(name="y", minimum=-5, maximum=5)

design_space = [design_1, design_2]

kpi_1 = KpiParameter("f1", KpiGoalMinimum)
kpi_2 = KpiParameter("f2", KpiGoalMaximum)

kpi_space = [kpi_1, kpi_2]

optimization_problem = OptimizationProblem(design_space, kpi_space)


class TestEnvironment(EnvironmentInterface):
    """
    Integration of the simulation environment.
    Takes the suggested designs as input and produces the kpis.
    """

    def evaluate(self, design_values: Dict[str, float]) -> Dict[str, float]:
        x = design_values["x"]
        y = design_values["y"]

        f1 = x ** 2
        f2 = y ** 2 - x

        return {"f1": f1, "f2": f2}


paretos = Paretos(config)

now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
name = f"example_{now}"

paretos.optimize(
    name=name,
    optimization_problem=optimization_problem,
    environment=TestEnvironment(),
    terminators=[RunTerminator(max_number_of_evaluations)],
)

result = paretos.obtain(name=name)

result.to_csv(path="example.csv")
