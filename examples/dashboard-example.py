from paretos import Config, Paretos

config = Config(
    dashboard_port="5000", data_source_name="sqlite:///examples/example_paretos.sqlite3"
)

paretos = Paretos(config)

paretos.show()
