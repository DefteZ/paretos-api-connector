import csv

from paretos import Config, Paretos

config = Config(customer_token="", data_source_name="sqlite:///paretos.sqlite3")

paretos = Paretos(config=config)

result = paretos.export(name="example")

with open("database-export-example.csv", "w", newline="", encoding="utf-8") as csv_file:
    fieldnames = [field for field in result[0]]

    csv_writer = csv.DictWriter(csv_file, fieldnames, delimiter=";")
    csv_writer.writeheader()

    csv_writer.writerows(result)
