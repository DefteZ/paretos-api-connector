import configparser
import os
from typing import Dict

from paretos import (
    Config,
    DesignParameter,
    EnvironmentInterface,
    KpiGoalMaximum,
    KpiGoalMinimum,
    KpiParameter,
    OptimizationProblem,
    Paretos,
)

init_config = configparser.ConfigParser()
config_file = os.path.realpath(os.path.join(os.getcwd(), "env.ini"))
init_config.read(config_file)
socrates_url = init_config["RESUME_TEST"]["SOCRATES_URL"].strip('"')
customer_token = init_config["RESUME_TEST"]["CUSTOMER_TOKEN"].strip('"')

config = Config(
    customer_token=customer_token,
    socrates_url=socrates_url,
)

design_1 = DesignParameter(name="x", minimum=-5, maximum=5)
design_2 = DesignParameter(name="y", minimum=-5, maximum=5)

design_space = [design_1, design_2]

kpi_1 = KpiParameter("f1", KpiGoalMinimum)
kpi_2 = KpiParameter("f2", KpiGoalMaximum)

kpi_space = [kpi_1, kpi_2]

optimization_problem = OptimizationProblem(design_space, kpi_space)


class TestEnvironment(EnvironmentInterface):
    """
    Class containing the function call to trigger new simulation runs
    """

    i = 1

    def evaluate(
        self,
        design_values: Dict[str, float],
    ) -> Dict[str, float]:
        self.i = self.i + 1

        if self.i > 7:
            raise Exception("Stop.")

        x = design_values["x"]
        y = design_values["y"]

        f1 = x ** 2
        f2 = y ** 2 - x

        return {"f1": f1, "f2": f2}


paretos = Paretos(config)


result = paretos.optimize(
    name="resume_test",
    optimization_problem=optimization_problem,
    environment=TestEnvironment(),
)
