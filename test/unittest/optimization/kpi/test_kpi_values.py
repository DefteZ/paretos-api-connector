from unittest import TestCase

from paretos.optimization.kpi import KpiParameter, KpiValues
from paretos.optimization.parameter import ParameterValue


class Test(TestCase):
    def test_instantiates(self):
        kpi_parameter_0 = KpiParameter(name="0")
        kpi_parameter_1 = KpiParameter(name="1")

        kpi_value_0 = ParameterValue(parameter=kpi_parameter_0, value=0.33)
        kpi_value_1 = ParameterValue(parameter=kpi_parameter_1, value=0.66)
        value_list = [kpi_value_0, kpi_value_1]

        kpi_values = KpiValues(values=value_list)

        self.assertIsInstance(kpi_values, KpiValues)
