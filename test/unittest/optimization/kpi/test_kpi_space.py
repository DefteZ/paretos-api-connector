from unittest import TestCase

from paretos.optimization.kpi import KpiParameter, KpiSpace


class Test(TestCase):
    def test_instantiates(self):
        kpi_parameter_0 = KpiParameter(name="0")
        kpi_parameter_1 = KpiParameter(name="1")

        space = KpiSpace(parameters=[kpi_parameter_0, kpi_parameter_1])

        self.assertIsInstance(space, KpiSpace)
