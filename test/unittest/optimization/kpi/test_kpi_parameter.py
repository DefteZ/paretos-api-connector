from unittest import TestCase

from paretos.optimization.goals import Minimum
from paretos.optimization.kpi import KpiParameter


class Test(TestCase):
    def test_provides_goal(self):
        name = "test"
        goal = Minimum()
        uuid = "id"

        parameter = KpiParameter(name=name, goal=goal, uuid=uuid)

        self.assertEqual(goal, parameter.get_goal())
