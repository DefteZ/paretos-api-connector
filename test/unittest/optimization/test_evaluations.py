from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization import Evaluation, Evaluations


class Test(TestCase):
    def setUp(self) -> None:
        self.__evaluation_0 = Mock(Evaluation)
        self.__evaluation_1 = Mock(Evaluation)

        self.__evaluation_0.is_pareto_optimal.return_value = False
        self.__evaluation_1.is_pareto_optimal.return_value = True

        self.__evaluation_list = [self.__evaluation_0, self.__evaluation_1]

        self.__evaluations = Evaluations(self.__evaluation_list)

    def test_provides_all_evaluations(self):
        self.__evaluations.get_evaluations()

        self.assertEqual(self.__evaluation_list, self.__evaluations.get_evaluations())

    def test_input_output_list_are_side_effect_free(self):
        original_list = self.__evaluation_list.copy()

        self.__evaluation_list.append(Mock())

        retrieved_list = self.__evaluations.get_evaluations()

        self.assertEqual(original_list, retrieved_list)

        retrieved_list.append(Mock())

        self.assertEqual(original_list, self.__evaluations.get_evaluations())

    def test_is_empty_by_default(self):
        evaluations = Evaluations()

        self.assertEqual([], evaluations.get_evaluations())
        self.assertEqual([], evaluations.get_pareto_optimal_evaluations())

    def test_provides_pareto_optimal_evaluations(self):
        pareto_optimal_evaluations = self.__evaluations.get_pareto_optimal_evaluations()

        self.assertEqual([self.__evaluation_1], pareto_optimal_evaluations)

    def test_receives_evaluations(self):
        evaluation = Mock()
        self.__evaluations.add_evaluation(evaluation)

        self.assertEqual(evaluation, self.__evaluations.get_evaluations()[-1])
