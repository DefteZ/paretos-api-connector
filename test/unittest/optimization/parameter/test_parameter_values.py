from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization.parameter import Parameter, ParameterValue, ParameterValues


class Test(TestCase):
    def setUp(self) -> None:
        self.__parameter_name = "x"
        self.__parameter_float_value = 0.66
        self.__parameter = Parameter(self.__parameter_name)

        self.__parameter_value_object = ParameterValue(
            parameter=self.__parameter, value=self.__parameter_float_value
        )

        self.__parameter_value_list = [self.__parameter_value_object]
        self.__parameter_values = ParameterValues(self.__parameter_value_list)

    def test_iterates(self):
        for i, parameter_value in enumerate(self.__parameter_values):
            self.assertEqual(self.__parameter_value_list[i], parameter_value)

    def test_provides_values(self):
        self.assertEqual(
            self.__parameter_value_list, self.__parameter_values.get_values()
        )

    def test_fetches_float_value_by_parameter_name(self):
        self.assertEqual(
            self.__parameter_float_value,
            self.__parameter_values.get_value_by_name(self.__parameter_name),
        )

    def test_exports_to_dict(self):
        self.assertEqual(
            {self.__parameter_name: self.__parameter_float_value},
            self.__parameter_values.to_dict(),
        )

    def test_input_and_output_list_is_side_effect_free(self):
        original_list = self.__parameter_value_list.copy()

        self.__parameter_value_list.append(Mock())

        received_list = self.__parameter_values.get_values()

        self.assertEqual(original_list, received_list)

        received_list.append(Mock())

        received_list_2 = self.__parameter_values.get_values()

        self.assertEqual(original_list, received_list_2)

    def test_non_existing_name_raises_value_error(self):
        with self.assertRaises(ValueError):
            self.__parameter_values.get_value_by_name("not existing")
