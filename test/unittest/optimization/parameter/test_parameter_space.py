from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization.parameter import Parameter, ParameterSpace


class Test(TestCase):
    def setUp(self) -> None:
        self.__name_0 = "name 0"
        self.__name_1 = "name 1"
        self.__id_0 = "id 0"
        self.__id_1 = "id 1"
        self.__parameter_0 = Parameter(name=self.__name_0, uuid=self.__id_0)
        self.__parameter_1 = Parameter(name=self.__name_1, uuid=self.__id_1)
        self.__parameter_list = [self.__parameter_0, self.__parameter_1]
        self.__size = len(self.__parameter_list)
        self.__space = ParameterSpace(self.__parameter_list)

    def test_provides_length(self):
        self.assertEqual(self.__size, self.__space.size())

    def test_fetches_parameter_by_id(self):
        self.assertEqual(
            self.__parameter_1, self.__space.get_parameter_by_id(self.__id_1)
        )

    def test_fetches_parameter_by_name(self):
        self.assertEqual(
            self.__parameter_1, self.__space.get_parameter_by_name(self.__name_1)
        )

    def test_iterates(self):
        for i, parameter in enumerate(self.__space):
            self.assertEqual(self.__parameter_list[i].get_id(), parameter.get_id())

    def test_input_list_is_side_effect_free(self):
        original_size = self.__size
        self.__parameter_list.append(Mock())

        size_after_modification = sum(1 for e in self.__space)

        self.assertEqual(original_size, size_after_modification)

    def test_no_match_returns_none(self):
        self.assertIsNone(self.__space.get_parameter_by_name("not existing"))

        self.assertIsNone(self.__space.get_parameter_by_id("not existing"))
