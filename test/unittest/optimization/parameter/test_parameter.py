from unittest import TestCase

from paretos.optimization.parameter import Parameter


class Test(TestCase):
    def test_provides_name_and_id(self):
        name = "test"
        uuid = "id"

        parameter = Parameter(name=name, uuid=uuid)

        self.assertEqual(name, parameter.get_name())
        self.assertEqual(uuid, parameter.get_id())

    def test_provides_default_uuid(self):
        parameter = Parameter(name="test")

        self.assertEqual(36, len(parameter.get_id()))
