from unittest import TestCase

from paretos.optimization.parameter import Parameter, ParameterValue


class Test(TestCase):
    def test_provides_value_and_parameter(self):
        parameter = Parameter("x")
        value = 0.66

        parameter_value = ParameterValue(parameter=parameter, value=value)

        self.assertEqual(value, parameter_value.get_value())
        self.assertEqual(parameter, parameter_value.get_parameter())
