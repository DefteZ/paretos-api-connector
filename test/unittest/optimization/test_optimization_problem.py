from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization import OptimizationProblem


class Test(TestCase):
    def test_provides_design_and_kpi_definitions(self):
        design_space = Mock()
        kpi_space = Mock()

        problem = OptimizationProblem(design_space=design_space, kpi_space=kpi_space)

        self.assertEqual(design_space, problem.get_design_space())
        self.assertEqual(kpi_space, problem.get_kpi_space())
