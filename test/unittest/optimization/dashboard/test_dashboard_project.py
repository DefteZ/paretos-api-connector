from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization.dashboard.dashboard_project import DashboardProject


class Test(TestCase):
    def test_instanciates_corretly(self):
        name = "foo"
        test_id = "bar"
        status = Mock()
        dashboard_project = DashboardProject(name, test_id, status)
        self.assertEqual(dashboard_project.get_id(), test_id)
        self.assertEqual(dashboard_project.get_name(), name)
        self.assertEqual(dashboard_project.get_status(), status)
