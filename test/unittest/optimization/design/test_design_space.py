from unittest import TestCase

from paretos.optimization.design import DesignParameter, DesignSpace


class Test(TestCase):
    def test_instantiates(self):
        parameters = [
            DesignParameter(name="x", minimum=0, maximum=1),
            DesignParameter(name="y", minimum=0, maximum=1),
        ]

        space = DesignSpace(parameters=parameters)

        self.assertIsInstance(space, DesignSpace)
