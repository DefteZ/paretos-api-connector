from unittest import TestCase

from paretos.optimization.design import DesignParameter, DesignValues
from paretos.optimization.parameter import ParameterValue


class Test(TestCase):
    def test_instantiates(self):
        parameters = [
            DesignParameter(name="x", minimum=0, maximum=1),
            DesignParameter(name="y", minimum=0, maximum=1),
        ]

        parameter_values = [
            ParameterValue(parameter=parameters[0], value=0.33),
            ParameterValue(parameter=parameters[1], value=0.66),
        ]

        design_values = DesignValues(values=parameter_values)

        self.assertIsInstance(design_values, DesignValues)
