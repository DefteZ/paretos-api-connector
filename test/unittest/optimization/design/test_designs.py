from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization.design import DesignParameter, Designs, DesignValues
from paretos.optimization.parameter import ParameterValue


class Test(TestCase):
    def test_provides_designs_and_iteration(self):
        parameters = [
            DesignParameter(name="x", minimum=0, maximum=1),
            DesignParameter(name="y", minimum=0, maximum=1),
        ]

        parameter_values_design_0 = [
            ParameterValue(parameter=parameters[0], value=0.33),
            ParameterValue(parameter=parameters[1], value=0.66),
        ]

        parameter_values_design_1 = [
            ParameterValue(parameter=parameters[0], value=0.1),
            ParameterValue(parameter=parameters[1], value=0.9),
        ]

        design_0 = DesignValues(values=parameter_values_design_0)
        design_1 = DesignValues(values=parameter_values_design_1)
        design_list = [design_0, design_1]

        designs = Designs(designs=design_list)

        self.assertEqual(design_list, designs.get_designs())

        for i, design in enumerate(designs):
            self.assertEqual(design_list[i], design)

    def test_is_empty_by_default(self):
        designs = Designs()

        self.assertEqual(0, len(designs.get_designs()))

    def test_input_and_output_list_is_side_effect_free(self):
        design_0 = Mock(DesignValues)
        design_1 = Mock(DesignValues)

        original_design_list = [design_0, design_1]
        input_design_list = original_design_list.copy()

        designs = Designs(designs=input_design_list)

        retrieved_design_list = designs.get_designs()

        retrieved_design_list.append(Mock(DesignValues))

        # changing the output list does not change the input list
        self.assertEqual(original_design_list, input_design_list)

        input_design_list.append(Mock(DesignValues))

        retrieved_design_list_2 = designs.get_designs()

        # changing the input list does not change the output list
        self.assertEqual(original_design_list, retrieved_design_list_2)
