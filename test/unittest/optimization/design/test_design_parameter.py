from unittest import TestCase

from paretos.optimization.design import DesignParameter


class Test(TestCase):
    def test_provides_boundaries(self):
        name = "test"
        minimum = 0
        maximum = 1

        parameter = DesignParameter(name=name, minimum=minimum, maximum=maximum)

        self.assertEqual(minimum, parameter.get_minimum())
        self.assertEqual(maximum, parameter.get_maximum())
