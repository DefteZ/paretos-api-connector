from unittest import TestCase

from paretos.optimization import DefaultTerminator
from paretos.optimization.progress import Progress


class Test(TestCase):
    def test_terminates_at_100(self):
        progress = Progress(100, 0)

        terminator = DefaultTerminator()

        should_terminate = terminator.should_terminate(progress)

        self.assertTrue(should_terminate)

    def test_does_not_terminate_before_100(self):
        progress = Progress(99, 99)

        terminator = DefaultTerminator()

        should_terminate = terminator.should_terminate(progress)

        self.assertFalse(should_terminate)
