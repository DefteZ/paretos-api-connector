from unittest import TestCase
from unittest.mock import Mock

from paretos.optimization import Evaluation


class Test(TestCase):
    def setUp(self) -> None:
        self.__design_values = Mock()
        self.__kpi_values = Mock()
        self.__is_pareto_optimal = False
        self.__uuid = "test"

        self.__evaluation = Evaluation(
            design=self.__design_values,
            kpis=self.__kpi_values,
            is_pareto_optimal=self.__is_pareto_optimal,
            uuid=self.__uuid,
        )

    def test_provides_id(self):
        self.assertEqual(self.__uuid, self.__evaluation.get_id())

    def test_provides_kpis(self):
        self.assertEqual(self.__kpi_values, self.__evaluation.get_kpis())

    def test_provides_design(self):
        self.assertEqual(self.__design_values, self.__evaluation.get_design())

    def test_provides_optimality_information(self):
        self.assertEqual(
            self.__is_pareto_optimal, self.__evaluation.is_pareto_optimal()
        )

    def test_receives_kpis(self):
        kpis = Mock()
        self.__evaluation.add_result(kpis=kpis)

        self.assertEqual(kpis, self.__evaluation.get_kpis())

    def test_receives_optimality_information(self):
        is_pareto_optimal = True
        self.__evaluation.update_pareto_optimality(is_pareto_optimal=is_pareto_optimal)

        self.assertEqual(is_pareto_optimal, self.__evaluation.is_pareto_optimal())
