from unittest import TestCase
from unittest.mock import Mock

from paretos import OptimizationProblem
from paretos.optimization import Project
from paretos.optimization.project_status import Done, Ready


class Test(TestCase):
    def setUp(self) -> None:
        self.__name = "test"
        self.__problem = Mock(OptimizationProblem)
        self.__status = Ready()
        self.__uuid = "003b03af-0531-4f59-bd4d-8ac24bc3e5ba"

        self.__project = Project(
            name=self.__name,
            problem=self.__problem,
            status=self.__status,
            uuid=self.__uuid,
        )

    def test_provides_id(self):
        self.assertEqual(self.__uuid, self.__project.get_id())

    def test_provides_status(self):
        self.assertIsInstance(self.__project.get_status(), Ready)

    def test_provides_name(self):
        self.assertEqual(self.__name, self.__project.get_name())

    def test_provides_problem_definition(self):
        self.assertEqual(self.__problem, self.__project.get_optimization_problem())

    def test_transitions_to_done(self):
        self.__project.finish()

        self.assertIsInstance(self.__project.get_status(), Done)
