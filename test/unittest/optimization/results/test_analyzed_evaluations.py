from unittest import TestCase

from paretos.optimization.results import AnalyzedEvaluation, AnalyzedEvaluations


class Test(TestCase):
    def test_returns_pareto_optimal_and_all_evaluation_ids(self):
        evaluation1 = AnalyzedEvaluation(id="1", is_pareto_optimal=False)
        evaluation2 = AnalyzedEvaluation(id="2", is_pareto_optimal=True)
        evaluations = [evaluation1, evaluation2]

        analyzed_evaluations = AnalyzedEvaluations(analyzed_evaluations=evaluations)

        all_evaluations = analyzed_evaluations.get_evaluation_ids()

        self.assertEqual(len(evaluations), len(all_evaluations))

        pareto_evaluations = analyzed_evaluations.get_pareto_evaluation_ids()

        self.assertEqual(1, len(pareto_evaluations))
        self.assertEqual(pareto_evaluations[0], "2")

    def test_instantiates_empty(self):
        analyzed_evaluations = AnalyzedEvaluations()

        all_evaluations = analyzed_evaluations.get_evaluation_ids()

        self.assertEqual(0, len(all_evaluations))

        pareto_evaluations = analyzed_evaluations.get_pareto_evaluation_ids()

        self.assertEqual(0, len(pareto_evaluations))
