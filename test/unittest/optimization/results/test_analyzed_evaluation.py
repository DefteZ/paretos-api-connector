from unittest import TestCase

from paretos.optimization.results import AnalyzedEvaluation


class Test(TestCase):
    def test_optimal_evaluation(self):
        evaluation_id = "test"
        is_pareto_optimal = True

        analyzed_evaluation = AnalyzedEvaluation(
            id=evaluation_id, is_pareto_optimal=is_pareto_optimal
        )

        self.assertEqual(evaluation_id, analyzed_evaluation.get_id())
        self.assertEqual(is_pareto_optimal, analyzed_evaluation.is_pareto_optimal())

    def test_non_optimal_evaluation(self):
        evaluation_id = "test"
        is_pareto_optimal = False

        analyzed_evaluation = AnalyzedEvaluation(
            id=evaluation_id, is_pareto_optimal=is_pareto_optimal
        )

        self.assertEqual(evaluation_id, analyzed_evaluation.get_id())
        self.assertEqual(is_pareto_optimal, analyzed_evaluation.is_pareto_optimal())
