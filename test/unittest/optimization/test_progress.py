from unittest import TestCase

from paretos.optimization import Progress


class Test(TestCase):
    def test(self):
        nr_of_evaluations = 10
        nr_of_pareto_points = 3

        progress = Progress(
            nr_of_evaluations=nr_of_evaluations, nr_of_pareto_points=nr_of_pareto_points
        )

        self.assertEqual(nr_of_evaluations, progress.get_nr_of_evaluations())

        self.assertEqual(nr_of_pareto_points, progress.get_nr_of_pareto_points())
