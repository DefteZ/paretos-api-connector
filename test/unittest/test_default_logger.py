import re
from io import StringIO
from unittest import TestCase

from paretos import DefaultLogger


class Test(TestCase):
    def test(self):
        stream = StringIO()

        logger = DefaultLogger(stream=stream)
        logger.info("test", extra={"something": 123})
        result = stream.getvalue()

        stream.close()

        parsed = self._parse(result)

        self.assertEqual("paretos", parsed["channel"])
        self.assertEqual("INFO", parsed["level"])
        self.assertEqual("test", parsed["message"])

    @staticmethod
    def _parse(data: str):
        expression = re.compile("^\\[([^]]+)] ([^.]+).([^:]+): ([^{]+) (.*)$")
        matcher = expression.match(data)
        groups = matcher.groups()

        return {
            "date": groups[0],
            "channel": groups[1],
            "level": groups[2],
            "message": groups[3],
            "json": groups[4],
        }
