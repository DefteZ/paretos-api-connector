from unittest import TestCase

from paretos import KpiGoalMaximum, KpiGoalMinimum, KpiParameter


class Test(TestCase):
    def test_provides_name(self):
        name = "test"

        kpi_parameter = KpiParameter(name=name)

        self.assertEqual(name, kpi_parameter.get_name())

    def test_provides_minimization_goal_by_default(self):
        name = "test"

        kpi_parameter = KpiParameter(name=name)

        self.assertEqual(KpiGoalMinimum, kpi_parameter.get_goal())

    def test_accepts_minimization_goal(self):
        name = "test"

        kpi_parameter = KpiParameter(name=name, goal=KpiGoalMinimum)

        self.assertEqual(KpiGoalMinimum, kpi_parameter.get_goal())

    def test_accepts_maximization_goal(self):
        name = "test"

        kpi_parameter = KpiParameter(name=name, goal=KpiGoalMaximum)

        self.assertEqual(KpiGoalMaximum, kpi_parameter.get_goal())

    def test_rejects_invalid_goal(self):
        name = "test"

        with self.assertRaises(ValueError):
            KpiParameter(name=name, goal="invalid")
