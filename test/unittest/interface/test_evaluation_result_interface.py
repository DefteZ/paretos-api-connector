from unittest import TestCase

from paretos import EvaluationResultInterface


class Test(TestCase):
    def test(self):
        interface = EvaluationResultInterface()

        with self.assertRaises(NotImplementedError):
            interface.is_pareto_optimal()

        with self.assertRaises(NotImplementedError):
            interface.get_evaluation_uuid()

        with self.assertRaises(NotImplementedError):
            interface.get_design_values()

        with self.assertRaises(NotImplementedError):
            interface.get_kpi_values()
