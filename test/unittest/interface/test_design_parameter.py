from unittest import TestCase

from paretos import DesignParameter


class Test(TestCase):
    def setUp(self) -> None:
        self.__name = "x"
        self.__minimum = 0
        self.__maximum = 1

        self.__design_parameter = DesignParameter(
            name=self.__name,
            minimum=self.__minimum,
            maximum=self.__maximum,
        )

    def test_provides_name(self):
        self.assertEqual(self.__name, self.__design_parameter.get_name())

    def test_provides_lower_boundary(self):
        self.assertEqual(self.__minimum, self.__design_parameter.get_minimum())

    def test_provides_upper_boundary(self):
        self.assertEqual(self.__maximum, self.__design_parameter.get_maximum())

    def test_rejects_invalid_range(self):
        with self.assertRaises(ValueError):
            DesignParameter(
                name="",
                minimum=1,
                maximum=0,
            )
