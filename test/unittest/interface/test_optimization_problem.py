from unittest import TestCase
from unittest.mock import Mock

from paretos import OptimizationProblem


class Test(TestCase):
    def setUp(self) -> None:
        self.__design_parameters = Mock()
        self.__kpi_parameters = Mock()

        self.__problem = OptimizationProblem(
            design_parameters=self.__design_parameters,
            kpi_parameters=self.__kpi_parameters,
        )

    def test_provides_design_space(self):
        self.assertEqual(
            self.__design_parameters, self.__problem.get_design_parameters()
        )

    def test_provides_kpi_space(self):
        self.assertEqual(self.__kpi_parameters, self.__problem.get_kpi_parameters())
