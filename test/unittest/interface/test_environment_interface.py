from unittest import TestCase
from unittest.mock import Mock

from paretos import EnvironmentInterface


class Test(TestCase):
    def test(self):
        interface = EnvironmentInterface()

        with self.assertRaises(NotImplementedError):
            interface.evaluate(design_values=Mock())
