from unittest import TestCase
from unittest.mock import Mock

from paretos import TerminatorInterface


class Test(TestCase):
    def test(self) -> None:
        terminator = TerminatorInterface()

        with self.assertRaises(NotImplementedError):
            terminator.should_terminate(progress=Mock())
