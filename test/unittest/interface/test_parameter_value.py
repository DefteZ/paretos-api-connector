from unittest import TestCase

from paretos import ParameterValue


class Test(TestCase):
    def setUp(self) -> None:
        self.__name = "x"
        self.__float_value = 0.5

        self.__parameter_value = ParameterValue(
            name=self.__name,
            value=self.__float_value,
        )

    def test_provides_name(self) -> None:
        self.assertEqual(self.__name, self.__parameter_value.get_name())

    def test_provides_value(self) -> None:
        self.assertEqual(self.__float_value, self.__parameter_value.get_value())
