from unittest import TestCase
from unittest.mock import Mock

from paretos import ProgressInterface, RunTerminator


class Test(TestCase):
    def test_does_not_stop_before_runs_reached(self) -> None:
        run_terminator = RunTerminator(number_of_runs=5)

        progress = Mock(ProgressInterface)
        progress.get_nr_of_evaluations.return_value = 4

        self.assertFalse(run_terminator.should_terminate(progress=progress))

    def test_stops_after_runs_reached(self) -> None:
        run_terminator = RunTerminator(number_of_runs=5)

        progress = Mock(ProgressInterface)
        progress.get_nr_of_evaluations.return_value = 5

        self.assertTrue(run_terminator.should_terminate(progress=progress))
