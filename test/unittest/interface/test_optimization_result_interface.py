from unittest import TestCase

from paretos import OptimizationResultInterface


class Test(TestCase):
    def test(self) -> None:
        optimization_result_interface = OptimizationResultInterface()

        with self.assertRaises(NotImplementedError):
            optimization_result_interface.to_dicts()

        with self.assertRaises(NotImplementedError):
            optimization_result_interface.to_csv("")

        with self.assertRaises(NotImplementedError):
            optimization_result_interface.get_pareto_optimal_evaluations()

        with self.assertRaises(NotImplementedError):
            optimization_result_interface.get_evaluations()
