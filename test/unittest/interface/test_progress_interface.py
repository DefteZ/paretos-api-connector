from unittest import TestCase

from paretos import ProgressInterface


class Test(TestCase):
    def test(self) -> None:
        progress_interface = ProgressInterface()

        with self.assertRaises(NotImplementedError):
            progress_interface.get_nr_of_evaluations()

        with self.assertRaises(NotImplementedError):
            progress_interface.get_nr_of_pareto_points()
