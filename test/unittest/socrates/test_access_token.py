from datetime import datetime, timedelta
from unittest import TestCase
from unittest.mock import MagicMock, patch

from paretos.socrates.AccessToken import TOKEN_BUFFER_SECONDS, AccessToken


class Test(TestCase):
    def setUp(self) -> None:
        self.__access_token = "test"
        self.__expires = datetime.fromisoformat("2021-03-18T13:41:17.653+00:00")

        self.__token_object = AccessToken(
            access_token=self.__access_token, expires=self.__expires
        )

    def test_provides_raw_jwt(self):
        self.assertEqual(self.__access_token, self.__token_object.get_access_token())

    @patch("paretos.socrates.AccessToken.datetime")
    def test_is_marked_expired_after_real_expiration(self, datetime_mock: MagicMock):
        now = self.__expires + timedelta(seconds=1)
        datetime_mock.now.return_value = now

        self.assertTrue(self.__token_object.is_token_expired())

    @patch("paretos.socrates.AccessToken.datetime")
    def test_is_marked_expired_before_real_expiration(self, datetime_mock: MagicMock):
        now = self.__expires - timedelta(seconds=TOKEN_BUFFER_SECONDS - 1)
        datetime_mock.now.return_value = now

        self.assertTrue(self.__token_object.is_token_expired())

    @patch("paretos.socrates.AccessToken.datetime")
    def test_is_not_marked_expired_before_buffered_expiration(
        self, datetime_mock: MagicMock
    ):
        now = self.__expires - timedelta(seconds=TOKEN_BUFFER_SECONDS + 1)
        datetime_mock.now.return_value = now

        self.assertFalse(self.__token_object.is_token_expired())
