from unittest import TestCase
from unittest.mock import Mock

from paretos import interface, optimization
from paretos.socrates.SocratesApiClient import SocratesApiClient
from paretos.socrates.SocratesApiHttpSession import SocratesApiHttpSession
from paretos.socrates.SocratesRequestMapper import SocratesRequestMapper
from paretos.socrates.SocratesResponseMapper import SocratesResponseMapper


class Test(TestCase):
    def setUp(self) -> None:
        self.__request_mapper = Mock(SocratesRequestMapper)
        self.__response_mapper = Mock(SocratesResponseMapper)
        self.__session = Mock(SocratesApiHttpSession)

        self.__socrates_api_client = SocratesApiClient(
            session=self.__session,
            request_mapper=self.__request_mapper,
            response_mapper=self.__response_mapper,
        )

        self.__problem = Mock(interface.OptimizationProblem)
        self.__evaluations = Mock(optimization.Evaluations)

        self.__request_evaluations = Mock()
        self.__request_problem = Mock()
        self.__response = Mock()

        self.__request_mapper.evaluations_to_request.return_value = (
            self.__request_evaluations
        )
        self.__request_mapper.problem_to_request.return_value = self.__request_problem
        self.__session.authenticated_request.return_value = self.__response

    def test_fetches_designs(self):
        quantity = 1
        expected_prediction = Mock()

        self.__response_mapper.predict_response_to_designs.return_value = (
            expected_prediction
        )

        prediction = self.__socrates_api_client.predict_design(
            problem=self.__problem, evaluations=self.__evaluations, quantity=quantity
        )

        self.__session.authenticated_request.assert_called_once_with(
            path="design/predict",
            version="v1",
            contains_sensitive_data=False,
            data={
                "problem": self.__request_problem,
                "evaluations": self.__request_evaluations,
                "quantity": quantity,
            },
        )

        self.__response_mapper.predict_response_to_designs.assert_called_once_with(
            problem=self.__problem, response_data=self.__response
        )

        self.assertEqual(expected_prediction, prediction)

    def test_tracks_progress(self):
        expected_progress = Mock()

        self.__response_mapper.track_response_to_progress.return_value = (
            expected_progress
        )

        progress = self.__socrates_api_client.track_progress(
            problem=self.__problem, evaluations=self.__evaluations
        )

        self.__session.authenticated_request.assert_called_once_with(
            path="progress/track",
            version="v1",
            contains_sensitive_data=False,
            data={
                "problem": self.__request_problem,
                "evaluations": self.__request_evaluations,
            },
        )

        self.__response_mapper.track_response_to_progress.assert_called_once_with(
            response_data=self.__response
        )

        self.assertEqual(expected_progress, progress)

    def test_filters_pareto_points(self):
        expected_evaluation_ids = ["3"]

        response = {
            "evaluations": [
                {"evaluationId": "1", "isParetoOptimal": False},
                {"evaluationId": "3", "isParetoOptimal": True},
            ]
        }

        self.__session.authenticated_request.return_value = response

        optimal_ids = self.__socrates_api_client.get_pareto_optimal_evaluation_ids(
            problem=self.__problem, evaluations=self.__evaluations
        )

        self.__session.authenticated_request.assert_called_once_with(
            path="result/analyze",
            version="v1",
            contains_sensitive_data=False,
            data={
                "problem": self.__request_problem,
                "evaluations": self.__request_evaluations,
            },
        )

        self.assertEqual(expected_evaluation_ids, optimal_ids)
