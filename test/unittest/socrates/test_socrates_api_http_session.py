from logging import LoggerAdapter
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch

from requests import Response

from paretos.socrates.SocratesApiHttpSession import SocratesApiHttpSession


class Test(TestCase):
    @patch("paretos.socrates.SocratesApiHttpSession.AccessToken")
    @patch("paretos.socrates.SocratesApiHttpSession.Session")
    def test(self, session_mock: MagicMock, access_token_mock: MagicMock):
        self.__api_url = "http://example.com"
        self.__customer_token = "test_token"
        self.__logger = Mock(LoggerAdapter)

        self.__session = SocratesApiHttpSession(
            api_url=self.__api_url,
            customer_token=self.__customer_token,
            logger=self.__logger,
        )

        access_token_mock.return_value.is_valid.return_value = True

        path = "unittest"
        version = "v1"
        contains_sensitive_data = False
        data = {}
        method = "POST"

        response_0 = Mock(Response)
        response_0.status_code = 200
        response_0.text = "dummy"

        response_0.json.return_value = {
            "status": "success",
            "data": {
                "accessToken": "dummy",
                "accessTokenExpiration": "2021-03-24T11:50:54+00:00",
            },
        }

        response_1 = Mock(Response)
        response_1.status_code = 200
        response_1.text = "dummy"

        expected_result = {"message": "unittest"}

        response_1.json.return_value = {"status": "success", "data": expected_result}

        session_mock.return_value.request.side_effect = [response_0, response_1]

        result = self.__session.authenticated_request(
            path=path,
            version=version,
            contains_sensitive_data=contains_sensitive_data,
            data=data,
            method=method,
        )

        self.assertEqual(expected_result, result)
