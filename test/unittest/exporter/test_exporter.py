import os
from unittest import TestCase

from paretos.exporter import export


class Test(TestCase):
    def test(self):
        fixture_file = os.path.join(
            os.path.dirname(__file__), "fixtures/exporter_unittest.sqlite3"
        )

        data_source_name = f"sqlite:///{fixture_file}"

        result = export(
            data_source_name=data_source_name, project_name="exporter_unittest"
        )

        self.assertEqual(10, len(result))
        self.assertEqual(-1.7663814339780797, result[0]["design__x"])
        self.assertEqual(3.120103370302457, result[0]["kpi__f1"])

    def test_processes_cancelled_optimization_data(self):
        fixture_file = os.path.join(
            os.path.dirname(__file__), "fixtures/cancelled_optimization.sqlite3"
        )

        data_source_name = f"sqlite:///{fixture_file}"

        result = export(
            data_source_name=data_source_name, project_name="cancelled_optimization"
        )

        self.assertEqual(10, len(result))
        self.assertEqual(-0.36395955955137627, result[0]["design__x"])
        self.assertEqual(0.1324665609888318, result[0]["kpi__f1"])

        # unfinished evaluation has no kpis
        self.assertFalse("kpi__f1" in result[1])
