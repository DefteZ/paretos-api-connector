from unittest import TestCase

from paretos.version import VERSION


class Test(TestCase):
    def test(self):
        self.assertIsInstance(VERSION, str)
        self.assertTrue(len(VERSION) > 0)
