from unittest import TestCase

from paretos import optimization
from paretos.export import OptimizationResult


class Test(TestCase):
    def setUp(self) -> None:
        f1 = optimization.kpi.KpiParameter(
            name="f1", uuid="0bc4fd62-d0e7-48f4-9cb4-d82ef5ed29fe"
        )
        f2 = optimization.kpi.KpiParameter(
            name="f2", uuid="bb62df60-3f63-4d39-b80b-43b2c2115592"
        )

        x1 = optimization.design.DesignParameter(
            name="x1", minimum=0, maximum=1, uuid="c4666afa-5497-48a8-9976-d30af5d1b1ec"
        )
        x2 = optimization.design.DesignParameter(
            name="x2", minimum=0, maximum=1, uuid="ada42ce2-900e-491b-b078-6c6aa3b2df0d"
        )

        x1_0 = optimization.parameter.ParameterValue(x1, 0.2)
        x2_0 = optimization.parameter.ParameterValue(x2, 0.4)
        f1_0 = optimization.parameter.ParameterValue(f1, 2)
        f2_0 = optimization.parameter.ParameterValue(f2, 4)
        optimal_0 = False
        design_values_0 = optimization.design.DesignValues(values=[x1_0, x2_0])
        kpi_values_0 = optimization.kpi.KpiValues(values=[f1_0, f2_0])
        evaluation_0 = optimization.Evaluation(
            design=design_values_0,
            kpis=kpi_values_0,
            is_pareto_optimal=optimal_0,
            uuid="873ef522-ba85-4d0d-bd6f-add45ada0013",
        )

        x1_1 = optimization.parameter.ParameterValue(x1, 0.3)
        x2_1 = optimization.parameter.ParameterValue(x2, 0.6)
        f1_1 = optimization.parameter.ParameterValue(f1, 3)
        f2_1 = optimization.parameter.ParameterValue(f2, 9)
        optimal_1 = True
        design_values_1 = optimization.design.DesignValues(values=[x1_1, x2_1])
        kpi_values_1 = optimization.kpi.KpiValues(values=[f1_1, f2_1])
        evaluation_1 = optimization.Evaluation(
            design=design_values_1,
            kpis=kpi_values_1,
            is_pareto_optimal=optimal_1,
            uuid="702f6795-6c94-488d-8789-f96c21dc014b",
        )

        evaluations = optimization.Evaluations([evaluation_0, evaluation_1])

        self.__result = OptimizationResult(evaluations=evaluations)

        self.__result_evaluations = self.__result.get_evaluations()
        self.__optimal_evaluations = self.__result.get_pareto_optimal_evaluations()

        self.__dicts = self.__result.to_dicts()

    def test_filters_pareto_optimal_evaluations(self):
        self.assertEqual(1, len(self.__optimal_evaluations))
        optimal_evaluation = self.__optimal_evaluations[0]
        design_values = optimal_evaluation.get_design_values()
        kpi_values = optimal_evaluation.get_kpi_values()
        self.assertTrue(optimal_evaluation.is_pareto_optimal())
        self.assertEqual(0.3, design_values["x1"])
        self.assertEqual(0.6, design_values["x2"])
        self.assertEqual(3, kpi_values["f1"])
        self.assertEqual(9, kpi_values["f2"])

    def test_returns_all_evaluations(self):
        self.assertEqual(2, len(self.__result_evaluations))

        evaluation_0 = self.__result_evaluations[0]
        design_values_0 = evaluation_0.get_design_values()
        kpi_values_0 = evaluation_0.get_kpi_values()
        self.assertFalse(evaluation_0.is_pareto_optimal())
        self.assertEqual(0.2, design_values_0["x1"])
        self.assertEqual(0.4, design_values_0["x2"])
        self.assertEqual(2, kpi_values_0["f1"])
        self.assertEqual(4, kpi_values_0["f2"])

        evaluation_1 = self.__result_evaluations[1]
        design_values_1 = evaluation_1.get_design_values()
        kpi_values_1 = evaluation_1.get_kpi_values()
        self.assertTrue(evaluation_1.is_pareto_optimal())
        self.assertEqual(0.3, design_values_1["x1"])
        self.assertEqual(0.6, design_values_1["x2"])
        self.assertEqual(3, kpi_values_1["f1"])
        self.assertEqual(9, kpi_values_1["f2"])

    def test_provides_flat_datastructure(self):
        self.assertEqual(2, len(self.__dicts))

        evaluation_0 = self.__dicts[0]

        self.assertFalse(evaluation_0["is_pareto_optimal"])
        self.assertEqual(0.2, evaluation_0["design__x1"])
        self.assertEqual(0.4, evaluation_0["design__x2"])
        self.assertEqual(2, evaluation_0["kpi__f1"])
        self.assertEqual(4, evaluation_0["kpi__f2"])

        evaluation_1 = self.__dicts[1]

        self.assertTrue(evaluation_1["is_pareto_optimal"])
        self.assertEqual(0.3, evaluation_1["design__x1"])
        self.assertEqual(0.6, evaluation_1["design__x2"])
        self.assertEqual(3, evaluation_1["kpi__f1"])
        self.assertEqual(9, evaluation_1["kpi__f2"])

    def test_provides_csv(self):
        self.__result.to_csv(":memory")

        with open(":memory", "r", newline="", encoding="utf-8") as csv_file:
            line_0 = csv_file.readline()
            line_1 = csv_file.readline()
            line_2 = csv_file.readline()

        # different line endings on different systems, so ignore them
        self.assertIn(
            "evaluation_id;is_pareto_optimal;design__x1;design__x2;kpi__f1;kpi__f2",
            line_0,
        )

        self.assertIn("873ef522-ba85-4d0d-bd6f-add45ada0013;False;0.2;0.4;2;4", line_1)

        self.assertIn("702f6795-6c94-488d-8789-f96c21dc014b;True;0.3;0.6;3;9", line_2)

    def test_evaluation_list_is_side_effect_free(self):
        retrieved_list = self.__result.get_evaluations()
        original_list = retrieved_list.copy()

        retrieved_list.append("something")

        self.assertEqual(original_list, self.__result.get_evaluations())

    def test_optimal_list_is_side_effect_free(self):
        retrieved_list = self.__result.get_pareto_optimal_evaluations()
        original_list = retrieved_list.copy()

        retrieved_list.append("something")

        self.assertEqual(original_list, self.__result.get_pareto_optimal_evaluations())
