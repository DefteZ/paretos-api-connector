from unittest import TestCase

from paretos.export import EvaluationResult


class Test(TestCase):
    def test(self):
        evaluation_uuid = ""
        design_values = {"x1": 0.2, "x2": 0.6}
        kpi_values = {"f1": 2, "f2": 2.3}
        is_pareto_optimal = False

        result = EvaluationResult(
            evaluation_uuid=evaluation_uuid,
            design_values=design_values,
            kpi_values=kpi_values,
            is_pareto_optimal=is_pareto_optimal,
        )

        self.assertEqual(evaluation_uuid, result.get_evaluation_uuid())
        self.assertEqual(design_values, result.get_design_values())
        self.assertEqual(kpi_values, result.get_kpi_values())
        self.assertEqual(is_pareto_optimal, result.is_pareto_optimal())
