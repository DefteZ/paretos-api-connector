from logging import Logger
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch

from paretos import Config, DefaultLogger
from paretos.exceptions import ConfigError


class Test(TestCase):
    def test_provides_default_configuration(self):
        customer_token = ""

        config = Config(
            customer_token=customer_token,
        )

        self.assertEqual(customer_token, config.get_customer_token())

        self.assertIsInstance(config.get_logger(), DefaultLogger)

        self.assertEqual("sqlite:///paretos.sqlite3", config.get_data_source_name())

        self.assertEqual("https://api.paretos.io/socrates/", config.get_api_url())

    def test_accepts_custom_logger(self):
        logger = Mock(Logger)

        config = Config(customer_token="", logger=logger)

        self.assertEqual(logger, config.get_logger())

    @patch("paretos.config.is_valid_url")
    def test_rejects_invalid_api_url(self, validator: MagicMock):
        validator.return_value = False

        with self.assertRaises(ConfigError):
            Config(customer_token="")

    def test_appends_trailing_slash_to_api_url(self):
        socrates_url = "https://api.paretos.io/socrates"

        config = Config(customer_token="", socrates_url=socrates_url)

        self.assertEqual("https://api.paretos.io/socrates/", config.get_api_url())
