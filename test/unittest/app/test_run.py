import unittest
from unittest.mock import MagicMock, Mock, patch

from paretos.app.run import number_of_workers, run


class Test(unittest.TestCase):
    @patch("paretos.app.run.multiprocessing")
    def test_number_of_workers(self, multiprocessing: MagicMock):
        multiprocessing.cpu_count.return_value = 2
        result = number_of_workers()
        multiprocessing.cpu_count.assert_called_once()
        self.assertEqual(result, 5)

    @patch("paretos.app.run.create_app")
    @patch("paretos.app.run.os_name", "nt")
    def test_run_win(self, create_app: MagicMock):
        data_source = Mock()
        config = Mock()
        app = Mock()
        create_app.return_value = app
        config.get_data_source_name.return_value = data_source
        run(config)
        config.get_data_source_name.assert_called_once()
        create_app.assert_called_once_with(data_source)
        app.run.assert_called_once()

    @patch("paretos.app.run.create_app")
    @patch("paretos.app.run.number_of_workers")
    @patch("paretos.app.run.import_application")
    @patch("paretos.app.run.logging")
    @patch("paretos.app.run.os_name", "posix")
    def test_run_linux(
        self,
        logging: MagicMock,
        import_application: MagicMock,
        n_workers: MagicMock,
        create_app: MagicMock,
    ):
        data_source = Mock()
        config = Mock()
        application_module = import_application.return_value
        application_module.Application = Mock()
        app = create_app.return_value
        config.get_data_source_name.return_value = data_source
        config.get_dashboard_host.return_value = "foo"
        config.get_dashboard_port.return_value = "bar"
        logging.getLevelName.return_value = "test"
        n_workers.return_value = 5
        config.get_data_source_name.return_value = data_source

        run(config)
        config.get_logger.assert_called_once()
        config.get_dashboard_host.assert_called_once()
        config.get_dashboard_port.assert_called_once()
        config.get_data_source_name.assert_called_once()
        create_app.assert_called_once_with(data_source)
        import_application.assert_called_once()
        application_module.Application.assert_called_once_with(
            app, {"bind": "foo:bar", "workers": 5, "loglevel": "test"}
        )
        application_module.Application.return_value.run.assert_called_once()
