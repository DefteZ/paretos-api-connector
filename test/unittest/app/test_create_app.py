import unittest
from unittest.mock import MagicMock, Mock, patch

from paretos.app.create_app import create_app


class Test(unittest.TestCase):
    @patch("paretos.app.create_app.RouteRegistry")
    @patch("paretos.app.create_app.Container")
    @patch("paretos.app.create_app.CORS")
    def test_create_app(
        self, CORS: MagicMock, container: MagicMock, route_registry: MagicMock
    ):
        cont = container.return_value
        route_reg = route_registry.return_value
        app = Mock()
        app.config = {}
        cont.App.return_value = app
        result = create_app("test")

        CORS.assert_called_once_with(app, origins="*", supports_credentials=True)
        cont.App.assert_called_once()
        route_registry.assert_called_once_with(cont)
        route_reg.register_routes.assert_called_once_with(app)
        self.assertEqual(result, app)
