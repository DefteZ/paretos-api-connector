import unittest
from unittest.mock import Mock

from dependency_injector.providers import Provider

from paretos.app.src.CommandHandler.CommandHandler import CommandHandler
from paretos.app.src.RouteRegistry import RouteRegistry

Route = "/pithos/v1/projects/get"
Handle = "Projects_Get"


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.container = Mock()
        self.app = Mock()
        self.add_url_rule = Mock()
        self.request_handler = Mock()
        self.view_function = Mock()
        self.get_view_function = Mock(return_value=self.view_function)

        self.container.getattr = Mock()
        self.container.RequestHandler = Mock(return_value=self.request_handler)
        self.app.add_url_rule = self.add_url_rule
        self.request_handler.get_view_function = self.get_view_function

    def test_registers_routes(self):
        registry = RouteRegistry(self.container)

        registry.register_routes(self.app)

        self.add_url_rule.assert_called()

    def test_registers_view_functions_as_they_are(self):
        request_handler = Mock()

        setattr(self.container, Handle, request_handler)

        registry = RouteRegistry(self.container)

        registry.register_routes(self.app)

        self.add_url_rule.assert_any_call(Route, view_func=request_handler)

    def test_resolves_providers_before_registration(self):
        handler = Mock()
        provider = Mock(Provider, return_value=handler)

        setattr(self.container, Handle, provider)

        registry = RouteRegistry(self.container)

        registry.register_routes(self.app)

        provider.assert_called()
        self.add_url_rule.assert_any_call(Route, view_func=handler)

    def test_wraps_command_handler_with_request_handler(self):
        command_handler = Mock(CommandHandler)

        setattr(self.container, Handle, command_handler)

        registry = RouteRegistry(self.container)

        registry.register_routes(self.app)

        self.get_view_function.assert_called_with(command_handler)
        self.add_url_rule.assert_any_call(Route, view_func=self.view_function)

    def test_resolves_provider_and_wraps_command_handler(self):
        command_handler = Mock(CommandHandler)
        provider = Mock(Provider, return_value=command_handler)

        setattr(self.container, Handle, provider)

        registry = RouteRegistry(self.container)

        registry.register_routes(self.app)

        provider.assert_called()
        self.add_url_rule.assert_any_call(Route, view_func=self.view_function)
