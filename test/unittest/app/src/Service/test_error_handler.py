import unittest
from unittest.mock import Mock, patch

from werkzeug.exceptions import InternalServerError

from paretos.app.src.Service.ErrorHandler import ErrorHandler


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.traceback_patcher = patch("paretos.app.src.Service.ErrorHandler.traceback")
        self.traceback = self.traceback_patcher.start()

        self.trace = ["test", "traceback"]
        self.internal_server_error = Mock(InternalServerError)
        self.message = "Test Unexpected Error"
        self.original_exception = Exception(self.message)

        self.traceback.format_exc = Mock(return_value=self.trace)
        self.internal_server_error.original_exception = self.original_exception

        self.application_protocol = Mock()
        self.logger = Mock()

    def tearDown(self) -> None:
        self.traceback_patcher.stop()

    def test_logs_exception(self):
        error_handler = ErrorHandler(
            application_protocol=self.application_protocol, logger=self.logger
        )

        error_handler.handle_error(self.internal_server_error)

        self.logger.error.assert_called_once_with(
            self.message, error={"trace": self.trace}
        )

    def test_creates_jsend_response(self):
        jsend = Mock()
        expected_response = Mock()
        build_error_response = Mock(return_value=expected_response)

        jsend.build_error_response = build_error_response

        error_handler = ErrorHandler(application_protocol=jsend, logger=self.logger)

        response, code = error_handler.handle_error(self.internal_server_error)

        self.assertEqual(500, code)
        self.assertEqual(expected_response, response)
