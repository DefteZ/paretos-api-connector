import unittest
from unittest.mock import Mock, patch

from werkzeug.exceptions import InternalServerError

from paretos.app.src.Service.ResponseMapper import DataApiResponseMapper
from paretos.optimization import Evaluation, Evaluations, OptimizationProblem
from paretos.optimization.design import DesignParameter, DesignSpace, DesignValues
from paretos.optimization.goals import Maximum, Minimum
from paretos.optimization.kpi import KpiParameter, KpiSpace, KpiValues
from paretos.optimization.parameter import Parameter, ParameterValue


class Test(unittest.TestCase):
    def test_evaluations_to_request_empty(self):
        evaluations = Evaluations([])
        request_evaluations = DataApiResponseMapper.evaluations_to_request(evaluations)
        self.assertIsInstance(request_evaluations, list)
        self.assertEqual(0, len(request_evaluations))

    def test_evaluations_to_request_with_evaluations(self):
        design_values = DesignValues([ParameterValue(Parameter(name="design"), 2.0)])
        kpi_values = KpiValues([ParameterValue(Parameter(name="kpi"), 3.0)])
        evalutation = Evaluation(
            design=design_values, kpis=kpi_values, is_pareto_optimal=True
        )
        evaluations = Evaluations([evalutation])

        request_evaluations = DataApiResponseMapper.evaluations_to_request(evaluations)

        self.assertIsInstance(request_evaluations, list)
        self.assertEqual(1, len(request_evaluations))

        request_evaluation = request_evaluations[0]

        self.assertIsInstance(request_evaluation, dict)
        self.assertIn("id", request_evaluation)

        self.assertIn("design", request_evaluation)
        request_designs = request_evaluation["design"]
        self.assertEqual(1, len(request_designs))
        self.assertIn("id", request_designs[0])
        self.assertEqual(request_designs[0]["value"], 2.0)

        self.assertIn("kpis", request_evaluation)
        request_kpis = request_evaluation["kpis"]
        self.assertEqual(1, len(request_kpis))
        self.assertIn("id", request_kpis[0])
        self.assertEqual(request_kpis[0]["value"], 3.0)

    def test_problem_to_request_minimize(self):
        design_space = DesignSpace(
            [DesignParameter(name="design", minimum=0, maximum=5, uuid="test")]
        )
        kpi_space = KpiSpace([KpiParameter(name="kpi", goal=Minimum(), uuid="test2")])
        problem = OptimizationProblem(design_space=design_space, kpi_space=kpi_space)

        request_problem = DataApiResponseMapper.problem_to_request(problem)

        self.assertDictEqual(
            request_problem,
            {
                "design": [
                    {"id": "test", "minimum": 0, "maximum": 5, "name": "design"}
                ],
                "kpis": [{"id": "test2", "goal": "minimize", "name": "kpi"}],
            },
        )

    def test_problem_to_request_maximize(self):
        design_space = DesignSpace([])
        kpi_space = KpiSpace([KpiParameter(name="kpi", goal=Maximum(), uuid="test2")])
        problem = OptimizationProblem(design_space=design_space, kpi_space=kpi_space)

        request_problem = DataApiResponseMapper.problem_to_request(problem)

        self.assertDictEqual(
            request_problem,
            {
                "design": [],
                "kpis": [{"id": "test2", "goal": "maximize", "name": "kpi"}],
            },
        )

    def test_problem_to_request_invalid_goal(self):
        design_space = DesignSpace([])
        kpi_space = KpiSpace([KpiParameter(name="kpi", goal=Mock(), uuid="test2")])
        problem = OptimizationProblem(design_space=design_space, kpi_space=kpi_space)

        with self.assertRaises(RuntimeError):
            DataApiResponseMapper.problem_to_request(problem)
