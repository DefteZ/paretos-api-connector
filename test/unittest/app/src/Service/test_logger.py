import datetime
import io
import json
import re
import unittest
from unittest.mock import Mock
from uuid import uuid4

import dateutil.parser

from paretos.app.src.Service import RequestIdProvider
from paretos.app.src.Service.Logger import Logger

TestMessage = "Test Log Message."
RequestId = "7b98487d-2252-4e76-96ad-7acb41670587"
ApiVersion = "0.TEST"
ExtraInfo = "Test Extra"
ExtraKey = "unittest-extra"

request_id_provider = Mock(RequestIdProvider)
request_id_provider.get_request_id = Mock(return_value=RequestId)


def log_test_info(channel: str = None) -> str:
    if channel is None:
        channel = str(uuid4())  # needs to be unique for concurrent unit tests

    stream = io.StringIO()
    logger = Logger(
        channel=channel,
        request_id_provider=request_id_provider,
        api_version=ApiVersion,
        stream=stream,
    )
    logger.info(TestMessage, **{ExtraKey: ExtraInfo})
    result = stream.getvalue()
    stream.close()

    return result


def log_and_parse(channel: str = None):
    log = log_test_info(channel=channel)
    expression = re.compile("^\\[([^]]+)] ([^.]+).([^:]+): ([^{]+) (.*)$")
    matcher = expression.match(log)
    groups = matcher.groups()

    return {
        "date": groups[0],
        "channel": groups[1],
        "level": groups[2],
        "message": groups[3],
        "json": groups[4],
    }


class Test(unittest.TestCase):
    def test_log_contains_message(self):
        log = log_test_info()

        self.assertTrue(log.find(TestMessage) > -1)

    def test_log_contains_iso8601_date(self):
        date_string = log_and_parse()["date"]
        date_parsed = dateutil.parser.isoparse(date_string)

        self.assertIsInstance(date_parsed, datetime.datetime)

    def test_log_contains_level(self):
        level = log_and_parse()["level"]

        self.assertEqual("INFO", level)

    def test_log_contains_channel(self):
        expected = "Test_Channel"
        channel = log_and_parse(channel=expected)["channel"]

        self.assertEqual(expected, channel)

    def test_log_contains_json_data(self):
        data_raw = log_and_parse()["json"]
        data = json.loads(data_raw)

        self.assertIsInstance(data, dict)

    def test_log_contains_request_id(self):
        data_raw = log_and_parse()["json"]
        data = json.loads(data_raw)

        self.assertIsInstance(data["meta"]["requestId"], str)

    def test_log_contains_contextual_info(self):
        data_raw = log_and_parse()["json"]
        data = json.loads(data_raw)

        self.assertEqual(data["data"][ExtraKey], ExtraInfo)
