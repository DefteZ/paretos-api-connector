import unittest

from paretos.app.src.Service.RequestIdProvider import RequestIdProvider


class Test(unittest.TestCase):
    def test_provides_id(self):
        request_id_provider = RequestIdProvider()
        request_id = request_id_provider.get_request_id()

        self.assertIsInstance(request_id, str)

    def test_provides_unique_ids(self):
        request_id_provider_0 = RequestIdProvider()
        request_id_provider_1 = RequestIdProvider()

        request_id_0 = request_id_provider_0.get_request_id()
        request_id_1 = request_id_provider_1.get_request_id()

        self.assertNotEqual(request_id_0, request_id_1)

    def test_provides_same_id_until_updated(self):
        request_id_provider = RequestIdProvider()
        request_id_0 = request_id_provider.get_request_id()
        request_id_1 = request_id_provider.get_request_id()
        request_id_provider.update()
        request_id_2 = request_id_provider.get_request_id()

        self.assertEqual(request_id_0, request_id_1)
        self.assertNotEqual(request_id_0, request_id_2)
