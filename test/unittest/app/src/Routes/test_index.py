import unittest
from unittest.mock import MagicMock, Mock, patch

from paretos.app.src.Routes.Index import Index


class Test(unittest.TestCase):
    @patch("paretos.app.src.Routes.Index.render_template")
    def test_redirects_to_documentation(self, render_template: MagicMock):
        handler = Index()

        expected = Mock()
        render_template.return_value = expected

        result = handler.dispatch_request()

        self.assertEqual(expected, result)
        render_template.assert_called_once()
