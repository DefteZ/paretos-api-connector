import unittest
from unittest.mock import MagicMock, Mock, patch

from paretos.app.src.CommandHandler.Outcome.NotFoundFailure import NotFoundFailure
from paretos.app.src.Routes.ProjectMeta.Get import Get


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.logger = Mock()
        self.persistence = Mock()

        self.handler = Get(logger=self.logger, persistence=self.persistence)

    def test_listens_on_post(self):
        methods = self.handler.get_methods()
        self.assertEqual(["POST"], methods)

    def test_provides_validation_schema(self):
        schema = self.handler.get_schema()
        self.assertTrue(schema)

    @patch("paretos.app.src.Routes.ProjectMeta.Get.DataApiResponseMapper")
    def test_returns_project_meta(self, response_mapper: MagicMock):
        request_data = {"project": "test"}

        problem = Mock()
        project = Mock()
        mapped_response = Mock()
        project.get_optimization_problem.return_value = problem
        response_mapper.problem_to_request.return_value = mapped_response
        self.persistence.load_project_data_by_id.return_value = (project, [])

        result = self.handler.process(request_data)

        self.persistence.load_project_data_by_id.assert_called_once_with("test")
        project.get_optimization_problem.assert_called_once()
        self.logger.info.assert_called_once()
        self.assertEqual(result, mapped_response)

    def test_returns_project_meta_not_found(self):
        request_data = {"project": "test"}
        self.persistence.load_project_data_by_id.return_value = None, []

        result = self.handler.process(request_data)

        self.assertIsInstance(result, NotFoundFailure)
