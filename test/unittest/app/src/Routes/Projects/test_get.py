import unittest
from unittest.mock import Mock

from paretos.app.src.Routes.Projects.Get import Get


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.logger = Mock()
        self.persistence = Mock()

        self.handler = Get(logger=self.logger, persistence=self.persistence)

    def test_listens_on_post(self):
        methods = self.handler.get_methods()
        self.assertEqual(["POST"], methods)

    def test_schema_is_none(self):
        schema = self.handler.get_schema()
        self.assertEqual(schema, None)

    def test_returns_project_meta(self):
        project = Mock()
        project.get_id.return_value = "foo"
        project.get_name.return_value = "bar"
        self.persistence.get_projects.return_value = [project]

        result = self.handler.process({})

        project.get_id.assert_called_once()
        project.get_name.assert_called_once()
        self.logger.info.assert_called_once()
        self.assertDictEqual(
            result, {"projects": [{"id": "foo", "name": "bar", "description": None}]}
        )
