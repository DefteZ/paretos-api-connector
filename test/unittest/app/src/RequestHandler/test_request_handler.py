import unittest
from unittest.mock import MagicMock, Mock, patch

import flask
from werkzeug.exceptions import BadRequest

from paretos.app.src.CommandHandler.CommandHandler import CommandHandler
from paretos.app.src.CommandHandler.Outcome.Failure import Failure
from paretos.app.src.CommandHandler.Outcome.Success import Success
from paretos.app.src.RequestHandler.InvalidRequestSchemaFailure import (
    InvalidRequestSchemaFailure,
)
from paretos.app.src.RequestHandler.ParsingJsonFailed import ParsingJsonFailed
from paretos.app.src.RequestHandler.RequestHandler import RequestHandler


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.application_protocol = Mock()
        self.logger = Mock()
        self.app = flask.Flask(__name__)

        self.command_handler_name = "unittest.command.handler"
        self.command_handler_methods = ["Post"]
        self.command_handler_schema = {
            "type": "object",
            "properties": {
                "someProperty": {"type": "string"},
            },
            "required": [
                "someProperty",
            ],
        }

        self.valid_request_data = {"someProperty": "someValue"}
        self.invalid_request_data = {"someOtherProperty": "some Value"}
        self.request_method_post = "POST"
        self.request_method_get = "GET"

        self.command_handling_outcome = {}
        self.success_response = Mock()
        self.fail_response = Mock()

        self.command_handler = Mock(CommandHandler)
        self.command_handler.get_name.return_value = self.command_handler_name
        self.command_handler.get_methods.return_value = self.command_handler_methods
        self.command_handler.get_schema.return_value = self.command_handler_schema
        self.command_handler.process.return_value = self.command_handling_outcome

        self.application_protocol.build_fail_response.return_value = self.fail_response
        self.application_protocol.build_success_response.return_value = (
            self.success_response
        )

        self.request_handler = RequestHandler(
            application_protocol=self.application_protocol,
            logger=self.logger,
        )

    def test_provides_flask_compatible_view_function(self):
        view_function = self.request_handler.get_view_function(self.command_handler)

        self.assertIn(self.command_handler_name, view_function.__name__)
        self.assertEqual(self.command_handler_methods, view_function.methods)

    def test_parses_json_on_post_request(self):
        with self.app.test_request_context("/", method=self.request_method_post):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = self.valid_request_data

            view_function = self.request_handler.get_view_function(self.command_handler)
            view_function()

            flask.request.get_json.assert_called_once()
            self.command_handler.process.assert_called_once_with(
                self.valid_request_data
            )

    def tests_rejects_invalid_request_data_structure(self):
        with self.app.test_request_context("/", method=self.request_method_post):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = self.invalid_request_data

            view_function = self.request_handler.get_view_function(self.command_handler)
            response, http_code = view_function()

            outcome_tuple = self.application_protocol.build_fail_response.call_args[0]
            self.assertIsInstance(outcome_tuple[0], InvalidRequestSchemaFailure)
            self.assertEqual(self.fail_response, response)
            self.assertEqual(400, http_code)

    def tests_catches_parsing_error_and_returns_jsend_failure(self):
        with self.app.test_request_context("/", method=self.request_method_post):
            flask.request.get_json = MagicMock()
            flask.request.get_json.side_effect = BadRequest()

            view_function = self.request_handler.get_view_function(self.command_handler)
            response, http_code = view_function()

            outcome_tuple = self.application_protocol.build_fail_response.call_args[0]
            self.assertIsInstance(outcome_tuple[0], ParsingJsonFailed)
            self.assertEqual(self.fail_response, response)
            self.assertEqual(400, http_code)

    def test_processes_get_request_without_data(self):
        with self.app.test_request_context("/", method=self.request_method_get):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = None

            view_function = self.request_handler.get_view_function(self.command_handler)
            response, http_code = view_function()

            self.assertEqual(self.success_response, response)
            self.assertEqual(200, http_code)

    def test_builds_response_from_fail_outcome(self):
        with self.app.test_request_context("/", method=self.request_method_get):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = self.valid_request_data

            failure = Failure()
            self.command_handler.process.return_value = failure

            view_function = self.request_handler.get_view_function(self.command_handler)
            response, http_code = view_function()

            self.application_protocol.build_fail_response.assert_called_once_with(
                failure
            )
            self.assertEqual(self.fail_response, response)
            self.assertEqual(failure.get_http_status_code(), http_code)

    def test_builds_response_from_success_outcome(self):
        with self.app.test_request_context("/", method=self.request_method_get):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = self.valid_request_data

            success_data = {}
            outcome = Success(success_data)
            self.command_handler.process.return_value = outcome

            view_function = self.request_handler.get_view_function(self.command_handler)
            response, http_code = view_function()

            self.application_protocol.build_success_response.assert_called_once_with(
                success_data
            )
            self.assertEqual(self.success_response, response)
            self.assertEqual(200, http_code)

    def test_builds_success_response_from_dict_outcome(self):
        with self.app.test_request_context("/", method=self.request_method_get):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = self.valid_request_data

            success_data = {}
            self.command_handler.process.return_value = success_data

            view_function = self.request_handler.get_view_function(self.command_handler)
            response, http_code = view_function()

            self.application_protocol.build_success_response.assert_called_once_with(
                success_data
            )
            self.assertEqual(self.success_response, response)
            self.assertEqual(200, http_code)

    def test_raises_error_on_unexpected_outcome_type(self):
        with self.app.test_request_context("/", method=self.request_method_get):
            flask.request.get_json = MagicMock()
            flask.request.get_json.return_value = self.valid_request_data

            self.command_handler.process.return_value = Mock()

            view_function = self.request_handler.get_view_function(self.command_handler)

            with self.assertRaises(NotImplementedError):
                view_function()
