import unittest
from unittest.mock import MagicMock, Mock, patch

from paretos.app.src.CommandHandler.Outcome.Failure import Failure
from paretos.app.src.RequestHandler.JSend import JSend
from paretos.app.src.Service.RequestIdProvider import RequestIdProvider


class Test(unittest.TestCase):
    @patch("paretos.app.src.RequestHandler.JSend.jsonify")
    def test_success_response(self, jsonify: MagicMock):
        request_id = "e4b80e32-da58-4c39-a633-899fd62495c4"
        api_version = "0.0.unittest"
        data = {"key": "value"}

        request_id_provider = Mock(RequestIdProvider)
        request_id_provider.get_request_id.return_value = request_id

        expected_response = Mock()
        jsonify.return_value = expected_response

        jsend = JSend(request_id_provider=request_id_provider, api_version=api_version)
        response_object = jsend.build_success_response(data=data)

        jsonify.assert_called_once()
        response_data = jsonify.call_args[0][0]

        self.assertEqual("success", response_data["status"])
        self.assertEqual(data, response_data["data"])
        self.assertEqual(api_version, response_data["meta"]["apiVersion"])
        self.assertEqual(request_id, response_data["meta"]["requestId"])
        self.assertEqual(expected_response, response_object)

    @patch("paretos.app.src.RequestHandler.JSend.jsonify")
    def test_failure_response(self, jsonify: MagicMock):
        request_id = "e4b80e32-da58-4c39-a633-899fd62495c4"
        api_version = "0.0.unittest"
        failure_id = "35973991-d7cd-4950-9b29-16861376c092"
        failure_message = "unit test"
        failure_details = {"key": "value"}

        failure = Mock(Failure)
        failure.get_id.return_value = failure_id
        failure.get_message.return_value = failure_message
        failure.get_details.return_value = failure_details

        request_id_provider = Mock(RequestIdProvider)
        request_id_provider.get_request_id.return_value = request_id

        expected_response = Mock()
        jsonify.return_value = expected_response

        jsend = JSend(request_id_provider=request_id_provider, api_version=api_version)
        response_object = jsend.build_fail_response(failure=failure)

        jsonify.assert_called_once()
        response_data = jsonify.call_args[0][0]

        self.assertEqual("fail", response_data["status"])
        self.assertEqual(failure_id, response_data["data"]["reason"])
        self.assertEqual(failure_message, response_data["data"]["description"])
        self.assertEqual(failure_details, response_data["data"]["details"])
        self.assertEqual(api_version, response_data["meta"]["apiVersion"])
        self.assertEqual(request_id, response_data["meta"]["requestId"])
        self.assertEqual(expected_response, response_object)

    @patch("paretos.app.src.RequestHandler.JSend.jsonify")
    def test_error_response(self, jsonify: MagicMock):
        request_id = "e4b80e32-da58-4c39-a633-899fd62495c4"
        api_version = "0.0.unittest"

        request_id_provider = Mock(RequestIdProvider)
        request_id_provider.get_request_id.return_value = request_id

        expected_response = Mock()
        jsonify.return_value = expected_response

        jsend = JSend(request_id_provider=request_id_provider, api_version=api_version)
        response_object = jsend.build_error_response()

        jsonify.assert_called_once()
        response_data = jsonify.call_args[0][0]

        self.assertEqual("error", response_data["status"])
        self.assertEqual("Internal server error.", response_data["message"])
        self.assertEqual(api_version, response_data["meta"]["apiVersion"])
        self.assertEqual(request_id, response_data["meta"]["requestId"])
        self.assertEqual(expected_response, response_object)
