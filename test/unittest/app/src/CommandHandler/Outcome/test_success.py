import unittest

from paretos.app.src.CommandHandler.Outcome.Success import Success


class Test(unittest.TestCase):
    def test_provides_data(self):
        expected = {"key": "value"}
        success = Success(data=expected)
        actual = success.get_data()

        self.assertEqual(actual, expected)
