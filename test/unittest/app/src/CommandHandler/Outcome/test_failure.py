import unittest

from paretos.app.src.CommandHandler.Outcome.Failure import Failure


class Test(unittest.TestCase):
    def test_provides_detail_info(self):
        expected = {"key": "value"}
        failure = Failure(details=expected)
        actual = failure.get_details()

        self.assertEqual(actual, expected)

    def test_provides_http_status(self):
        self.assertEqual(200, Failure().get_http_status_code())

    def test_provides_message(self):
        message = Failure().get_message()
        self.assertIsInstance(message, str)

    def test_provides_id(self):
        id_ = Failure().get_id()
        self.assertIsInstance(id_, str)
