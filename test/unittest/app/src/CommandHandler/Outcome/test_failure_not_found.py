import unittest

from paretos.app.src.CommandHandler.Outcome.NotFoundFailure import NotFoundFailure


class Test(unittest.TestCase):
    def test_provides_detail_info(self):
        expected = {"key": "value"}
        failure = NotFoundFailure(details=expected)
        actual = failure.get_details()

        self.assertEqual(actual, expected)

    def test_provides_http_status(self):
        self.assertEqual(404, NotFoundFailure().get_http_status_code())

    def test_provides_message(self):
        message = NotFoundFailure().get_message()
        self.assertIsInstance(message, str)

    def test_provides_id(self):
        id_ = NotFoundFailure().get_id()
        self.assertIsInstance(id_, str)
