import unittest

from paretos.app.src.CommandHandler.CommandHandler import CommandHandler


class Test(unittest.TestCase):
    def test_provides_name(self):
        command_handler = CommandHandler()
        name = command_handler.get_name()
        self.assertIsInstance(name, str)

    def test_provides_schema(self):
        command_handler = CommandHandler()
        schema = command_handler.get_schema()
        self.assertEqual(schema, None)

    def test_defines_http_method(self):
        command_handler = CommandHandler()
        methods = command_handler.get_methods()
        self.assertEqual("POST", methods[0])

    def test_processes_data(self):
        command_handler = CommandHandler()
        outcome = command_handler.process({"key": "value"})
        self.assertEqual({}, outcome)
